<?php


namespace App\Repositories\Interfaces;


interface SurveyInterface
{
    public function getNextQuestionByAnswerId($answerId);
}

<?php


namespace App\Repositories;


use App\Models\Answer;
use App\Repositories\Interfaces\SurveyInterface;

final class SurveyRepository implements SurveyInterface
{
    const AnswerIsNotFound = 100;
    const NoMoreQuestion = 101;

    public function getNextQuestionByAnswerId($answerId): array
    {
        // TODO: обработать отсутствующий answerId

        $answer = Answer::with(['nextQuestion', 'question.nextQuestion'])->where('id', $answerId)->first();

        if (!$answer) {
            throw new \Exception("The answer with answer_id = {$answerId} is not found", $this::AnswerIsNotFound);
        }

        // Получаем следующий вопрос либо из ответа либо (если в ответе не указан параметр next_question_id) из вопроса
        $nextQuestionObj = $answer->nextQuestion ?: $answer->question->nextQuestion;

        if ($nextQuestionObj) {
            $nextQuestion = $nextQuestionObj->toArray();
        } else {
            throw new \Exception("It was the last question", $this::NoMoreQuestion);
        }

        return $nextQuestion;
    }
}

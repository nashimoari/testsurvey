<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    function survey() {
        return $this->belongsTo(Survey::class);
    }

    function answers() {
        return $this->hasMany(Answer::class);
    }

    function nextQuestion() {
        return $this->hasOne(Question::class, 'id', 'next_question_id');
}
}

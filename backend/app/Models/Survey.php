<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    function questions() {
        $this->hasMany(Question::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    function question() {
        return $this->belongsTo(Question::class);
    }

    function nextQuestion() {
        return $this->hasOne(Question::class, 'id','next_question_id');
    }
}

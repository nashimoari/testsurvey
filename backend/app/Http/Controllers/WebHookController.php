<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\SurveyInterface;
use App\Services\Survey\Survey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebHookController extends Controller
{
    private $surveyRepo;

    public function __construct(SurveyInterface $surveyRepo)
    {
        $this->surveyRepo = $surveyRepo;
    }

    public function input(Request $request)
    {
        try {
            $result = ['status' => 'ok'];
            $survey = new Survey();

            // Валидируем входные данные внутри основного класса, а не контроллера. Так как обращение к классу может идти
            // не только через http запросы, но и через другие источники
            $survey->validation('getNextQuestion', $request->all());

            // Запрашиваем данные через репозиторий
            //$surveyRepo = new SurveyRepository();

            try {
                $data = $this->surveyRepo->getNextQuestionByAnswerId($request->get('answerId'));
                $result['response'] = ['code' => 'ok', 'data' => $data];
            } catch (\Throwable $e) {
                $result['response'] = ['code' => $e->getCode(), 'message' => $e->getMessage()];
            }

        } catch (\Throwable $e) {
            Log::debug($e->getTraceAsString());
            Log::debug($e->getMessage());
            $result['status'] = 'fail';
            $result['message'] = $e->getMessage();
        }
        return $result;


    }
}

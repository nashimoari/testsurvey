<?php


namespace App\Services\Survey;


use App\Services\Interfaces\CalledByController;
use Illuminate\Support\Facades\Log;

final class Survey implements CalledByController
{

    public function validation($type, $data)
    {
        switch ($type) {
            case 'getNextQuestion':
                if (!isset($data['answerId'])) {
                    throw new \Exception('answerId is absent');
                }
                break;

            default:
                throw new \Exception('Unknown type of validation');
                break;
        }


        Log::debug(print_r($data, 1));
        // TODO: Implement validation() method.
    }

}

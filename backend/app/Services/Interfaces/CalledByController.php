<?php


namespace App\Services\Interfaces;

use Illuminate\Http\Request;

/**
 * Для сервисов которые вызываются в контроллерах
 * @package App\Services\Interfaces
 */
interface CalledByController
{
    /**
     * Валидация входных данных
     * @param string $type тип валидации
     * @param array $data Данные которые нужно провалидировать
     * @return mixed
     */
    public function validation(string $type, $data);
}

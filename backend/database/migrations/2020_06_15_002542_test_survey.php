<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TestSurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $survey = [
            ['id' => 1, 'survey_name' => 'test']
        ];

        $questions = [
            ['id' => 2, 'survey_id' => 1, 'question_order' => 2, 'question_text' => 'Это второй вопрос ?', 'next_question_id' => null],
            ['id' => 1, 'survey_id' => 1, 'question_order' => 1, 'question_text' => 'Это первый вопрос ?', 'next_question_id' => 2],
            ['id' => 3, 'survey_id' => 1, 'question_order' => 3, 'question_text' => 'Это третий вопрос ?', 'next_question_id' => null],
        ];

        $answers = [
            ['question_id' => 1, 'answer_text' => 'Это первый ответ', 'next_question_id' => null],
            ['question_id' => 1, 'answer_text' => 'Это второй ответ', 'next_question_id' => null],
            ['question_id' => 1, 'answer_text' => 'Это третий ответ', 'next_question_id' => null],

            ['question_id' => 2, 'answer_text' => 'Это первый ответ', 'next_question_id' => 3],
            ['question_id' => 2, 'answer_text' => 'Это второй ответ', 'next_question_id' => 3],
            ['question_id' => 2, 'answer_text' => 'Это третий ответ', 'next_question_id' => 3],

            ['question_id' => 3, 'answer_text' => 'Это первый ответ', 'next_question_id' => null],
            ['question_id' => 3, 'answer_text' => 'Это второй ответ', 'next_question_id' => null],
            ['question_id' => 3, 'answer_text' => 'Это третий ответ', 'next_question_id' => null],
        ];

        DB::table('surveys')->insert($survey);
        DB::table('questions')->insert($questions);
        DB::table('answers')->insert($answers);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('answers')->delete();
        DB::table('questions')->where('survey_id', '=', 1)->delete();
        DB::table('surveys')->where('id', '=', 1)->delete();
    }

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('question_text')->nullable(false);
            $table->unsignedBigInteger('survey_id');
            $table->unsignedBigInteger('next_question_id')->nullable(true);
            $table->unsignedSmallInteger('question_order')->nullable(false);
            $table->foreign('survey_id')->references('id')->on('surveys');
            $table->foreign('next_question_id')->references('id')->on('questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}

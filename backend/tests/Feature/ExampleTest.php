<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testGetNexQuestionWhenAbsentAnswerId()
    {
        $response = $this->json('POST', '/api/v1/survey/getnextquestion', []);
        //$response->dump();
        $response->assertStatus(200)
            ->assertJson([
                'status' => 'fail',
                'message' => 'answerId is absent'])
        ;
    }

    public function testGetNexQuestionWhenIncorrectAnswerId()
    {
        $response = $this->json('POST', '/api/v1/survey/getnextquestion', ['answerId' => 0]);
        //$response->dump();
        $response->assertStatus(200)
            ->assertJson([
                'status' => 'ok',
                'response' => ['code' => 100]
            ])
        ;
    }

    public function testGetNextQuestionWhenFirst()
    {
        $response = $this->json('POST', '/api/v1/survey/getnextquestion', ['answerId' => 1]);
        //$response->dump();
        $response->assertStatus(200)
            ->assertJson([
                'status' => 'ok',
                'response' => ['code' => 'ok', 'data' => ['question_order' => 2]
                ]
            ])
        ;


    }

    public function testGetNextQuestionWhenSecond()
    {
        $response = $this->json('POST', '/api/v1/survey/getnextquestion', ['answerId' => 5]);
        //$response->dump();
        $response->assertStatus(200)
            ->assertJson([
                'status' => 'ok',
                'response' => ['code' => 'ok', 'data' => ['question_order' => 3]
                ]
            ])
        ;


    }

    public function testGetNexQuestionWhenLast()
    {
        $response = $this->json('POST', '/api/v1/survey/getnextquestion', ['answerId' => 7]);
        //$response->dump();
        $response->assertStatus(200)
            ->assertJson([
                'status' => 'ok',
                'response' => ['code' => 101]
            ])
        ;

    }
}
